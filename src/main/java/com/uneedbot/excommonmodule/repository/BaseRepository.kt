package com.uneedbot.excommonmodule.repository

import com.uneedbot.excommonmodule.Networking.NetworkService
import com.uneedbot.excommonmodule.Networking.Service


open class BaseRepository<T : Service<T2>, T2 : NetworkService> {

    protected var service : T? = null

    constructor(service : T)
    {
        this.service = service
    }


}