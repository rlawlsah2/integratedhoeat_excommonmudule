package com.uneedbot.excommonmodule.repository

import com.uneedbot.excommonmodule.Exceptions.EmailNotRegisteredException
import com.uneedbot.excommonmodule.Exceptions.LoginFailedException
import com.uneedbot.excommonmodule.Exceptions.LoginInfoNotFoundException
import com.uneedbot.excommonmodule.model.LoginData
import com.uneedbot.excommonmodule.Networking.NetworkService
import com.uneedbot.excommonmodule.Networking.Response.LoginResponse
import com.uneedbot.excommonmodule.Networking.Service
import io.reactivex.Observable
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody


/**
 * created by jmkim 2019.07.10
 * Login 데이터를 관장
 * */
open class LoginRepository<T : Service<T1>, T1 : NetworkService>(service: T) : BaseRepository<T, T1>(service) {

    companion object {

        var loginInfo: LoginData? = null
        private var INSTANCE: LoginRepository<*, *>? = null

        fun <T : Service<T1>, T1 : NetworkService> getInstance(service: T): LoginRepository<T, T1>? {

            if (INSTANCE == null) {
                INSTANCE = LoginRepository(service)
            }

            return INSTANCE as LoginRepository<T, T1>

        }
    }


    //로그인 데이터

    /**
     * token + tableNo 방식 로그인
     */
    fun getLoginInfo(token: String, tableNo: String): Observable<LoginData> {

        ///1. 로컬에 저장된 로그인 정보가 없는지 확인할것.
        if (loginInfo != null) {
            return Observable.just(loginInfo)
        } else {
            ///2. 로컬에 없다면 서버에서 가져와야한다
            return service!!.getLoginInfo(token, tableNo)
                    .subscribeOn(Schedulers.io())
                    .onErrorResumeNext(Function { error -> Observable.error(LoginFailedException("")) })
                    .flatMap { response ->
                        if (response.result?.branchInfo == null) {
                            return@flatMap Observable.error<LoginData>(LoginFailedException(""))
                        } else {
                            loginInfo = response.result
                            return@flatMap Observable.just(response.result)
                        }
                    }

        }
    }


    fun goLoginWithGoogleAccount(email: String): Observable<LoginData>? {
        return this.service?.loginWithGooleAccount(email)
                ?.subscribeOn(Schedulers.io())
                ?.flatMap {
                    if (it.status != null && it.status.code?.equals("E2000")) {
                        return@flatMap Observable.error<LoginData>(EmailNotRegisteredException())
                    }

                    Observable.just(it.result)
                }
    }


    ////점원용 로그인
    fun loginWithStaffIdPw(id: String, password: String): Observable<LoginData>? {
        return this.service?.loginWithStaffIdPw(id, password)?.flatMap {
            if (it.status != null && it.status.code?.equals("E2000")) {
                return@flatMap Observable.error<LoginData>(LoginInfoNotFoundException())
            }
            Observable.just(it.result)
        }
    }


}