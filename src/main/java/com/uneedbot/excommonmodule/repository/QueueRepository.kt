package com.uneedbot.excommonmodule.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.uneedbot.excommonmodule.Exceptions.LoginFailedException
import com.uneedbot.excommonmodule.Exceptions.PostRequestFailedException
import com.uneedbot.excommonmodule.Exceptions.order.CategoryNotFoundException
import com.uneedbot.excommonmodule.Networking.NetworkService
import com.uneedbot.excommonmodule.Networking.Response.OrderResultResponse
import com.uneedbot.excommonmodule.Networking.Response.TableOrderListResponse
import com.uneedbot.excommonmodule.Networking.Service
import com.uneedbot.excommonmodule.model.*
import com.uneedbot.excommonmodule.utils.LogUtil
import com.uneedbot.excommonmodule.utils.LoginUtil
import io.reactivex.Observable
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception
import java.util.ArrayList


/**
 * created by jmkim 2019.11.28
 * 주문을 제외한 큐에 연동관련 데이터
 * */
open class QueueRepository<T : Service<T1>, T1 : NetworkService>(service: T) : BaseRepository<T, T1>(service) {


    companion object {

        internal var mTableSettingList: LinkedHashMap<String, TableSetting>? = LinkedHashMap()

        private var INSTANCE: QueueRepository<*, *>? = null

        fun <T : Service<T1>, T1 : NetworkService> getInstance(service: T): QueueRepository<T, T1>? {

            if (INSTANCE == null) {
                INSTANCE = QueueRepository(service)
            }

            return INSTANCE as QueueRepository<T, T1>

        }
    }


    /**
     * 테이블 셋팅 목록 가져오기
     * */
    fun getTableSettingList(): Observable<LinkedHashMap<String, TableSetting>>? {

        if (mTableSettingList == null || mTableSettingList?.size ?: 0 == 0) {
            try {
                LogUtil.d("getTableSettingList before callOptions: " + LoginUtil.getInstance()?.loginInfo?.branchInfo?.callOptions!!)
                LoginUtil.getInstance()?.loginInfo?.branchInfo?.callOptions?.let { callOptions ->

                    val items = callOptions.entrySet().iterator()
                    val gson = Gson()
                    while (items.hasNext()) {
                        items.next()?.let {
                            LogUtil.d("getTableSettingList mid item: " + it)

                            mTableSettingList?.put(
                                    it.key, gson.fromJson(it.value, TableSetting::class.java)
                            )
                        }
                    }

                    LogUtil.d("getTableSettingList after callOptions: " + mTableSettingList)
                }
            } catch (e: Exception) {
                LogUtil.d("getTableSettingList error : " + e)
                e.printStackTrace()

                mTableSettingList = LinkedHashMap()
            }

        }
        LogUtil.d("getTableSettingList result : " + mTableSettingList)

        return Observable.just(mTableSettingList)
    }


    fun request(url: String, notification: Notification): Observable<Boolean> {
        return this.service?.request(url, notification)
                ?.flatMap {
                    if (it == null) {
                        return@flatMap Observable.just(false)
                    } else {
                        return@flatMap Observable.just(true)
                    }
                }!!

    }

}