package com.uneedbot.excommonmodule.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.uneedbot.excommonmodule.Exceptions.LoginFailedException
import com.uneedbot.excommonmodule.Exceptions.PostRequestFailedException
import com.uneedbot.excommonmodule.Exceptions.order.CategoryNotFoundException
import com.uneedbot.excommonmodule.Networking.NetworkService
import com.uneedbot.excommonmodule.Networking.Response.OrderResultResponse
import com.uneedbot.excommonmodule.Networking.Response.TableOrderListResponse
import com.uneedbot.excommonmodule.Networking.Service
import com.uneedbot.excommonmodule.model.*
import com.uneedbot.excommonmodule.utils.LogUtil
import io.reactivex.Observable
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList


/**
 * created by jmkim 2019.08.10
 * 주문에 관한 모든 데이터를 관리
 * */
open class OrderRepository<T : Service<T1>, T1 : NetworkService>(service: T) : BaseRepository<T, T1>(service) {


    companion object {

        internal var categoryRoot = LinkedHashMap<String, Category>()  //카테고리_대분류   키, 하위 카테고리 목록
        internal var categorySub = LinkedHashMap<String, ArrayList<Category>>()  //카테고리_중분류   키, 하위 카테고리 목록
        internal var categorySub2 = LinkedHashMap<String, ArrayList<Category>>()  //카테고리_소분류   키, 하위 카테고리 목록
        internal var menuList = LinkedHashMap<String, ArrayList<Menu>>()   //메뉴 목록. 키: 소분류의 카테고리 id / 값 : 메뉴명


        private var INSTANCE: OrderRepository<*, *>? = null

        fun <T : Service<T1>, T1 : NetworkService> getInstance(service: T): OrderRepository<T, T1>? {

            if (INSTANCE == null) {
                INSTANCE = OrderRepository(service)
            }

            return INSTANCE as OrderRepository<T, T1>

        }
    }


    /**
     * 대분류 카테고리 조회하기
     * */
    fun getRootCategories(storeBranchUid: String): Observable<LinkedHashMap<String, Category>>? {
        if (categoryRoot.size > 0) {
            return Observable.just(categoryRoot)
        } else {
            return this.service?.getRootCategory(storeBranchUid)
                    ?.subscribeOn(Schedulers.io())
                    ?.flatMap {

                        it?.result?.categories?.let {

                            for (category in it) {
                                categoryRoot.put(category.categoryCode, category)
                            }
                        }

                        if (categoryRoot.size == 0) {
                            return@flatMap Observable.error<LinkedHashMap<String, Category>>(
                                    CategoryNotFoundException()
                            )
                        }


                        return@flatMap Observable.just(categoryRoot)
                    }
        }
    }


    /**
     * 하위 카테고리 조회하기
     * @param categoryLevel 카테고리 레벨. 1:중분류/2:하위분류
     * */
    fun getSubCategories(categoryLevel: Int, categoryId: String, storeBranchUid: String): Observable<ArrayList<Category>>? {

        if (categoryLevel == 1) {
            if (categorySub.size > 0 && categorySub.get(categoryId)?.size ?: 0 > 0) {
                return Observable.just(categorySub.get(categoryId))
            } else {
                return this.service?.getSubCategoryList(categoryId, storeBranchUid)
                        ?.subscribeOn(Schedulers.io())
                        ?.flatMap {
                            it?.result?.categories?.let { categories ->
                                categorySub.put(categoryId, categories)
                            }

                            if (categorySub.size == 0) {
                                return@flatMap Observable.error<ArrayList<Category>>(
                                        CategoryNotFoundException()
                                )
                            }
                            return@flatMap Observable.just(categorySub.get(categoryId))
                        }
            }

        } else {
            if (categorySub2.size > 0 && categorySub2.get(categoryId)?.size ?: 0 > 0) {
                return Observable.just(categorySub2.get(categoryId))
            } else {
                return this.service?.getSubCategoryList(categoryId, storeBranchUid)
                        ?.subscribeOn(Schedulers.io())
                        ?.flatMap {
                            it?.result?.categories?.let { categories ->
                                categorySub2.put(categoryId, categories)
                            }

                            if (categorySub2.size == 0) {
                                return@flatMap Observable.error<ArrayList<Category>>(
                                        CategoryNotFoundException()
                                )
                            }
                            return@flatMap Observable.just(categorySub2.get(categoryId))
                        }
            }
        }
    }


    /**
     * 메뉴 목록 가져오기
     * */
    open fun getMenuList(categoryId: String, storeBranchUid: String): Observable<ArrayList<Menu>>? {

        return this.service?.getMenuList(categoryId, storeBranchUid)
                ?.subscribeOn(Schedulers.io())
                ?.flatMap {
                    return@flatMap Observable.just(it.result?.menus)
                }

    }

    /**
     * 메뉴 주문하기
     * */
    open fun order(url: String, tableNo: String, loginData: LoginData, member: Member?, cart: ArrayList<Pair<Category, Menu>>): Observable<OrderResultResponse>? {
        LogUtil.d("주문전 데이터 : " + cart)
//        var orderJson = JsonObject()
        ///카트에 담긴 아이템들을 불러온다
        val orderObject = JSONObject()
        val memberJsonObject = JSONObject()

        orderObject.put("userId", tableNo)
        orderObject.put("brandName", loginData?.branchInfo?.fbBrandName)
        orderObject.put("branchId", loginData?.branchInfo?.branchId)
        orderObject.put("tableNo", tableNo)
        orderObject.put("orderType", "H")

        member?.let {

            memberJsonObject.put("men", it.men)
            memberJsonObject.put("women", it.women)
            memberJsonObject.put("age", it.age)
        }
        orderObject.put("members", memberJsonObject)
        var totalPrice = 0

        val orders = JSONArray()

        cart?.forEach { item ->
            val ordersItem = JSONObject()   ///orders에 들어갈 각 항목
            val ordersItemSideOptions = JSONArray()

            item?.second?.let { menu ->

                ordersItem.put("foodUid", menu.foodUid)
                ordersItem.put("foodName", menu.foodName)
                ordersItem.put("quantity", menu.quantity)
                ordersItem.put("CMDT_CD", menu.CMDTCD)
                ordersItem.put("categoryName", menu.categoryName)

                var currentOptionPrice = 0 //현재 옵션 총 합산 가격

                ///옵션 여부 체크
                if (menu.isSet == 1) {
                    menu?.selectedOption?.forEach { selectedOptionGroup ->

                        selectedOptionGroup?.value?.forEach { selectedOptionItem ->

                            val ordersItemSideOptionItem = JSONObject()   ///orders에 들어갈 각 항목의 옵션
                            ordersItemSideOptionItem.put("foodUid", selectedOptionItem.foodUid)
                            ordersItemSideOptionItem.put("foodName", selectedOptionItem.foodName)
                            ordersItemSideOptionItem.put("price", selectedOptionItem.price)
                            ordersItemSideOptionItem.put("quantity", selectedOptionItem.quantity)
                            ordersItemSideOptionItem.put("CMDT_CD", selectedOptionItem.CMDTCD)

                            currentOptionPrice += selectedOptionItem.price * selectedOptionItem.quantity
                            ordersItemSideOptions.put(ordersItemSideOptionItem)

                        }

                        ///해당 옵션 총 가격 * 수량
                        totalPrice += currentOptionPrice * menu.quantity
                    }
                    if (ordersItemSideOptions.length() > 0) {
                        ordersItem.put("options", ordersItemSideOptions)
                    }
                } else {
                    ///해당 단품 가격 * 수량
                    totalPrice += menu.price * menu.quantity
                }
                ordersItem.put("price", menu.price)

                orders.put(ordersItem)
            }
        }


        orderObject.put("orders", orders)
        orderObject.put("totalPrice", totalPrice)

        LogUtil.d("주문 정보 : " + orderObject.toString())
        return this.service?.order(url, orderObject.toString())
    }

    /**
     * 해당 테이블의 멤버를 셋팅한다.
     * */
    fun setTableMembers(url: String, member: Member): Observable<TableOrderInfo>? {

        val membersJsonObject = JSONObject()
        val memberJsonObject = JSONObject()
        member?.let {
            memberJsonObject.put("men", it.men)
            memberJsonObject.put("women", it.women)
            memberJsonObject.put("age", it.age)
        }
        membersJsonObject.put("members", memberJsonObject)
        return this.service?.setTableMembers(url, membersJsonObject.toString())
                ?.flatMap {
                    if (it.result != null && it.result?.table != null) {
                        return@flatMap Observable.just(it.result?.table)
                    } else {
                        return@flatMap Observable.error<TableOrderInfo>(PostRequestFailedException())
                    }
                }
    }

//
//    /**
//     * 테이블 메뉴 가져오기
//     */
//    fun getTableInfo(url: String): Observable<TableOrderListResponse>? {
//        return this.service?.getTableInfo(url)
//    }


    /**
     * 주문한 내역을 가져오기
     */
    fun getOrderedList(url: String): Observable<TableOrderListResponse>? {
        return this.service?.getTableInfo(url)
    }


}