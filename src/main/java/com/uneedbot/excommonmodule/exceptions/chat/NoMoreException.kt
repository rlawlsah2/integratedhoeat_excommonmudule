package com.uneedbot.excommonmodule.Exceptions.chat

import com.uneedbot.excommonmodule.utils.Utils

/**
 * Created by kimjinmo on 2020.02
 * 채팅 대화 없
 */

class NoMoreException : RuntimeException {
    constructor(message: String? = "") : super(message)
}
