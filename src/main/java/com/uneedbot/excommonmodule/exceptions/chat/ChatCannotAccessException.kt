package com.uneedbot.excommonmodule.Exceptions.chat

import com.uneedbot.excommonmodule.utils.Utils

/**
 * Created by kimjinmo on 2020.02
 * 채팅방 접근 불가
 */

class ChatCannotAccessException : RuntimeException {
    constructor(message: String? = "") : super(message)
}
