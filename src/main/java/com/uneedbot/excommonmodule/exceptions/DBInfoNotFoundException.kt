package com.uneedbot.excommonmodule.Exceptions

/**
 * Created by kimjinmo on 2018. 7. 3..
 */

class LoginFailed(message: String) : Exception(message)
