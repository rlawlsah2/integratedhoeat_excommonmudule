package com.uneedbot.excommonmodule.Exceptions

/**
 * 로그인 정보를 가져오지 못한 경우
 * */
class LoginInfoNotFoundException(message : String = "") : Exception(message)
