package com.uneedbot.excommonmodule.Exceptions

/**
 * 해당 이메일이 등록되지않은경우
 * */
class EmailNotRegisteredException(message : String = "") : Exception(message)
