package com.uneedbot.excommonmodule.Exceptions.order

/**
 * 등록된 카테고리가 없거나 불러오지 못한경우.
 * */
class CategoryNotFoundException(message : String? = null) : Exception(message)
