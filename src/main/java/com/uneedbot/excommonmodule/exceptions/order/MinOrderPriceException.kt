package com.uneedbot.excommonmodule.Exceptions.order

import com.uneedbot.excommonmodule.utils.Utils

/**
 * Created by kimjinmo on 2019.8.5
 */

class MinOrderPriceException : RuntimeException {
    constructor(minOrderPrice: Int) : super("최소 " + Utils.setComma(minOrderPrice) + "원 이상 주문하셔야 합니다.")
}
