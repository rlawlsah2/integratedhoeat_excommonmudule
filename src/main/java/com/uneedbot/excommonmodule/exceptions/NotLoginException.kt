package com.uneedbot.excommonmodule.Exceptions

/**
 * 로그인하지않은 경우
 * */
class NotLoginException(message : String? = null) : Exception(message)
