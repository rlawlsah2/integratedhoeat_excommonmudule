package com.uneedbot.excommonmodule.Exceptions

/**
 * 데이터가 없는경우
 * */
class DataNotFoundException(message : String? = null) : Exception(message)
