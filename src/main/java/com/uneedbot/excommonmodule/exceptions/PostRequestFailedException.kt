package com.uneedbot.excommonmodule.Exceptions

/**
 * api 요청중 post인 경우 실패한 경우에 발생
 * */
class PostRequestFailedException(message : String? = null) : Exception(message)
