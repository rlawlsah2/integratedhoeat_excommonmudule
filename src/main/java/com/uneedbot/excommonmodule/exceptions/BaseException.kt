package com.uneedbot.excommonmodule.Exceptions

/**
 * Created by kimjinmo on 2018. 7. 3..
 */

class LoginFailedException(message: String) : Exception(message)
