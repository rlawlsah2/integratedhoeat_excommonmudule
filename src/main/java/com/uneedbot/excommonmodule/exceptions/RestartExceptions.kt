package com.uneedbot.excommonmodule.Exceptions

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

class RestartExceptions(message: String) : RuntimeException(message)
