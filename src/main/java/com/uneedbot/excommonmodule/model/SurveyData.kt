package com.uneedbot.excommonmodule.model


/**
 * Created by kimjinmo on 2017. 4. 28..
 */
data class SurveyData(
        var map: LinkedHashMap<String, ArrayList<String>>
) 