package com.uneedbot.excommonmodule.model


import com.google.gson.annotations.SerializedName

data class AppManager(
        @SerializedName("background")
        var background: String,
        @SerializedName("buttonArea")
        var buttonArea: String,
        @SerializedName("borderColor")
        var borderColor: String = "#40ffffff"
) : BaseData()
