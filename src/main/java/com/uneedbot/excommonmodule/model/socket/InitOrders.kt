package com.uneedbot.excommonmodule.model.socket

import com.google.gson.annotations.SerializedName
import com.uneedbot.excommonmodule.model.BaseData
import com.uneedbot.excommonmodule.model.Order

/**
 * Created by kimjinmo on 2019.08.13
 * kds에서 소켓으로 처음 주문데이터를 가져올때 사용
 */
public data class InitOrders(
        @SerializedName("orders")
        val orders: ArrayList<Order>,
        @SerializedName("state")
        val state: Int
) : BaseData()
