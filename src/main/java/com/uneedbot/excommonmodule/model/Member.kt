package com.uneedbot.excommonmodule.model

import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by kimjinmo on 2019.08.30
 */
data class Member(
        var men: Int = 0,
        var women: Int = 0,
        var age: Int = 0
) : BaseData()

