package com.uneedbot.excommonmodule.model


/**
 * Created by kimjinmo on 2018. 8. 1..
 */

data class UnivInfo(
        var storeBranchUid: String,
        var fbBrandName: String,
        var univName: String,
        var univUid: Int = 0,
        var commission: Double = 0.toDouble(),
        var logo: String,
        var appLayout: AppLayout
) : BaseData() 