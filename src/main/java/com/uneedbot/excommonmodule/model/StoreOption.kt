package com.uneedbot.excommonmodule.model


/**
 * Created by kimjinmo on 2018. 8. 1..
 */

data class StoreOption(
        var posUse: Boolean = false,
        var adShowInterval: Int = 600,//광고 보여지는 시간. 기본 10분단위
        var advertisement: Boolean = false,//광고 사용여부
        var present: Boolean = false,//선물하기 사용여부
        var tableChat: Boolean = false,//1:1 채팅 사용여부
        var storeChat: Boolean = false,//전체채팅 사용여부
        var kitchenTab: Boolean = false,//주문현황페이지 표시여부(사용안함)
        var survey: Boolean = false,//설문 페이지 사용여부
        var tableSetting: Boolean = false,//테이블 셋팅 사용여부
        var showMenuDetail: Boolean = false,//랜덤박스 사용여부
        var minOrderPrice: Int = 0, //최소주문금액
        var posType: String,
        var appType: String,  //앱 타입에따른 분기 처리


        //    "gameServerApi": "http://admin.hoeat.co.kr:5000",  // 게임 생성, 거절 API
        //            "gameServer": "ws://admin.hoeat.co.kr:5001"  // 실제 게임 접속 경로 (게임 앱에 전달할 경로)
        var gameServerApi: String,
        var gameServer: String
) : BaseData()


//
//public boolean posUse;
//public boolean tutorial = true;
//public int adShowInterval = 600;
//public boolean advertisement;
//public boolean present;
//public boolean tableChat;
//public boolean storeChat;
//public boolean kitchenTab;
//public boolean survey;
//public boolean tableSetting;
//public boolean showMenuDetail;
//public boolean randombox;
//public int randomboxPrice = 0;
//public int minOrderPrice = 0;       //최소주문금액 기능 사용여부
//public String posType;              //포스 타입. OAK기본
//public String appType;  //앱 타입에따른 분기 처리
//public ArrayList<String> lang;
//
//
////    "gameServerApi": "http://admin.hoeat.co.kr:5000",  // 게임 생성, 거절 API
////            "gameServer": "ws://admin.hoeat.co.kr:5001"  // 실제 게임 접속 경로 (게임 앱에 전달할 경로)
//public String gameServerApi;
//public String gameServer;
