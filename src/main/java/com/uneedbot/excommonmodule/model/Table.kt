package com.uneedbot.excommonmodule.model

import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by kimjinmo on 2019.08.30
 */
data class Table(
        var tNo: String,
        var x: Int = 0,
        var y: Int = 0,
        var width: Int = 0,
        var height: Int = 0,
        var hallOrder: Int = 0,
        var tableNo: String,
        var orderId: String,
        var isLocked : Boolean = false,
        var members : Member = Member()


        ) : BaseData()
