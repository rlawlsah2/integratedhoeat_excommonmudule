package com.uneedbot.excommonmodule.model

import com.google.gson.annotations.SerializedName

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
data class LoginData(
        @SerializedName("apiKey")
        var apiKey: String,
        @SerializedName("accessToken")
        var accessToken: String,
        @SerializedName("branchInfo")
        var branchInfo: BranchInfo
) : BaseData() 
