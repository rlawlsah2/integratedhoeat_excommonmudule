package com.uneedbot.excommonmodule.model

import java.io.Serializable

/**
 * Created by kimjinmo on 2016. 11. 16..
 */

open class BaseData : Cloneable, Serializable{

    public fun cloneClass(): BaseData {
        return super.clone() as BaseData
    }
}
