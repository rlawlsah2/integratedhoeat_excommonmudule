package com.uneedbot.excommonmodule.model

import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap

/**
 * Created by kimjinmo on 2019.08.30
 */
data class TableOrderInfo(
        var orderId: String,
        var orderNo: Int = 0,
        var userId: String,
        var tableNo: String,
        var movedFrom: String,
        var isFirstOrder : Boolean = false,
        var isPosOrder : Boolean = false,
        var orderRef: String,
        var totalPrice: Int = 0,
        var orders : LinkedHashMap<String, Order> = LinkedHashMap(),
        var members : Member = Member(),
        var orderType: String,
        var USERID: String,
        var STBL_CD: String,
        var saleDate: String,
        var SA_NO: Int = 0

        ) : BaseData()


