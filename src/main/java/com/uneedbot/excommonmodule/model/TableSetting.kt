package com.uneedbot.excommonmodule.model

import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap

/**
 * Created by kimjinmo on 2019.11.28
 * 테이블 셋팅에 사용하는 데이터
 */
data class TableSetting(
        var name: String,
        var value: String,
        var imageUrl: String,
        var useFlag: String

        ) : BaseData()


