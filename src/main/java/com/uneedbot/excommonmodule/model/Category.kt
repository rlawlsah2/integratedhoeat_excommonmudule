package com.uneedbot.excommonmodule.model

import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by kimjinmo on 2019.08.10
 * 메뉴 카테고리
 */
data class Category(
        var categoryCode: String = "",
        var categoryName: String= "",
        var parentCategory: String= "",
        var categoryDescription: String= "",
        var showDisabled: Boolean = true, ///
        var storeBranchUid: Int = 0,
        var categoryOrder: Int = 0,
        var childType: String= ""
) : BaseData()
