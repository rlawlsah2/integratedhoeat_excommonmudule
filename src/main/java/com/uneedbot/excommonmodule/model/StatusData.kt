package com.uneedbot.excommonmodule.model


/**
 * Created by kimjinmo on 2017. 4. 28..
 */
data class StatusData(
        var code: String,
        var message: String
) 