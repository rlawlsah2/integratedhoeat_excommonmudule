package com.uneedbot.excommonmodule.model

import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Created by kimjinmo on 2019.07.23
 */
data class Order(


        var menuUid: Int = 0,
        var foodUid: Int = 0,
        var sideGroupUid: Int = 0,
        var order: Int = 0,
        var foodImage: String,
        var foodName: String,
        var description: String,
        var tag: String,
        var CMDTCD: String,
        var categoryName: String,
        var price: Int = 0,
        var quantity:Int = 1,
        var soldOut: Int = 0,
        var isKitchen: Int = 0,
        var defaultItem: Int = 0,
        var showDetail: Int = 0,
        var maxQuantity: Int = 0,
        var isMenu:Boolean = true, ///앱에서 구분하기위한 용도로 쓰임.
        var isDivider:Boolean = false, ///앱에서 구분하기위한 용도로 쓰임.
        var isSet: Int = 0,
        var state: Int = 0,
        var tableNo : String,   ///해당 주문을 한 테이블 번호
        var orderItemKey : String,   ///해당 주문 KDS용 키
        var orderId : String,   ///해당 주문 KDS용 키
        var categoryCode:String,
        var discount: Int = 0,  //할인용


        var hashKey: String,
        @Nullable @SerializedName("options") var sideOptions:HashMap<String,Order> = HashMap(),


        ///상태 값.
        var newFlag: Int = 0,
        var hotFlag: Int = 0,
        var recommendFlag: Int = 0,
        var useFlag: Int = 0,
        var time : String
) : BaseData()
