package com.uneedbot.excommonmodule.model

data class OrderApp(
        var background: String,
        var backgroundColor: String,
        var buttonColor: String,
        var adHowTo: String,
        var adBackgroundColor: String,
        var adBackground: String,
        var featuredImage1: String,
        var featuredImage2: String,
        var featuredImage3: String,
        var featuredImage4: String,
        var featuredImage5: String
) : BaseData()
