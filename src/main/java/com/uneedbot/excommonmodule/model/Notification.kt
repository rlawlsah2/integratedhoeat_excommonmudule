package com.uneedbot.excommonmodule.model

import java.util.ArrayList
import java.util.Random


/**
 * Created by kimjinmo on 2019.11.20
 * 테이블 셋팅 요청할때
 */
class Notification(
        var content: String,
        var read: Boolean,
        var tableNo: String,
        var time: String,
        var title: String,
        var type: String,
        var artist: String? = null,
        var musicTitle: String? = null,
        var extra: Extra? = null
) : BaseData() {
    class Extra : BaseData() {
        var ref: String? = null
        var reference: String? = null
        var reason: String? = null
        var orderId: String? = null
    }
}


