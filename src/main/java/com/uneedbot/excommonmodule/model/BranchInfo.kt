package com.uneedbot.excommonmodule.model


import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

data class BranchInfo(
        @SerializedName("storeBranchUid")
        var storeBranchUid: String,
        @SerializedName("branchName")
        var branchName: String,
        @SerializedName("likes")
        var likes: Int = 0,
        @SerializedName("isLiking")
        var isLiking: Boolean = false,
        @SerializedName("univInfo")
        var univInfo: UnivInfo,
        @SerializedName("managerName")
        var managerName: String,
        @SerializedName("branchTel")
        var branchTel: String,
        @SerializedName("staffUid")
        var staffUid: String,
        @SerializedName("posIp")
        var posIp: String,
        @SerializedName("gameServerHost")
        var gameServerHost: String,
        @SerializedName("branchId")
        var branchId: String,
        @SerializedName("tablets")
        var tablets: Int = 0,
        @SerializedName("fbBrandName")
        var fbBrandName: String,
        @SerializedName("storeOption")
        var storeOption: StoreOption,
        @SerializedName("callOptions")
        var callOptions: JsonObject
) : BaseData() 