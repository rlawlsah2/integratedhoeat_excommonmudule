package com.uneedbot.excommonmodule.model

import com.google.gson.annotations.SerializedName

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

data class AppLayout(
        @SerializedName("appManager") val appManager: AppManager,
        @SerializedName("orderApp") val orderApp: OrderApp) : BaseData()