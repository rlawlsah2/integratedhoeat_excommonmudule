package com.uneedbot.excommonmodule.model.login

import com.google.gson.annotations.SerializedName
import com.uneedbot.excommonmodule.model.BaseData

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
data class LoginRequest(
        @SerializedName("apiKey")
        val apiKey: String,
        @SerializedName("tableNo")
        val tableNo: String,
        @SerializedName("branchName")
        val branchName: String
) : BaseData()
