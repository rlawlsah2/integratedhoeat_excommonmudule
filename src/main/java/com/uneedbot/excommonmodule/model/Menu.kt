package com.uneedbot.excommonmodule.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Created by kimjinmo on 2019.07.23
 */
data class Menu(
        var menuUid: Int = 0,
        var foodUid: Int = 0,
        var sideGroupUid: Int = 0,
        var order: Int = 0,
        var foodImage: String= "",
        var foodName: String= "",
        var description: String= "",
        var tag: String= "",
        var CMDTCD: String= "",
        var categoryName: String= "",
        var price: Int = 0,
        var quantity: Int = 1,
        var soldOut: Int = 0,
        var isKitchen: Int = 0,
        var defaultItem: Int = 0,
        var showDetail: Int = 0,
        var maxQuantity: Int = 0,
        var isMenu: Boolean = true, ///앱에서 구분하기위한 용도로 쓰임.
        var isDivider: Boolean = false, ///앱에서 구분하기위한 용도로 쓰임.
        var isSet: Int = 0,
        var state: Int = 0,

        var hashKey: String= "",
        var sideOptions: ArrayList<Menu> = ArrayList(),
        @SerializedName("sideGroup") var sideGroup: ArrayList<MenuSideGroup> = ArrayList(),


        ///상태 값.
        var newFlag: Int = 0,
        var hotFlag: Int = 0,
        var recommendFlag: Int = 0,
        var useFlag: Int = 0




        , var selectedOption: HashMap<MenuSideGroup?, HashSet<Menu>?>? = HashMap()  ///선택된 옵션 메뉴 처리할때 쓰는 변수. key : 해당 옵션의 구분, value : 구분 내에서 선택된 목록 set
) : BaseData()