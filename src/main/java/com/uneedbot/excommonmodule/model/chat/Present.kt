package com.uneedbot.excommonmodule.model.chat

import com.uneedbot.excommonmodule.model.BaseData

/**
 * Created by kimjinmo on 2020.02.17
 * 채팅방 정보 - 선물
 */
data class Present(
        var time: String = "",
        var menuName: String= "",
        var menuImage: String= "",
        var message: String= "",
        var read: Boolean = true,
        var fromTableNo: String= "",
        var key: String= ""
) : BaseData()

