package com.uneedbot.excommonmodule.model.chat

import com.uneedbot.excommonmodule.model.BaseData

/**
 * Created by kimjinmo on 2020.02.17
 * 채팅방 정보
 */
data class ChatRoom(
        var date: String = "",
        var message: String= "",
        var read: Boolean = true,
        var receiver: String= "",
        var sender: String= "",
        var time: String= "",
        var timeStamp: Long= 0
) : BaseData()

