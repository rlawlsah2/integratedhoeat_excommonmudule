package com.uneedbot.excommonmodule.model.chat

import com.uneedbot.excommonmodule.model.BaseData

/**
 * Created by kimjinmo on 2020.02.17
 * 채팅 내용
 */
data class ChatMessage(
        var date: String = "",
        var message: String= "",
        var read: Boolean = false,
        var receiver: String= "",
        var sender: String= "",
        var time: String= "",
        var timeStamp: Long= 0,
        var present: Present? = null
) : BaseData()


