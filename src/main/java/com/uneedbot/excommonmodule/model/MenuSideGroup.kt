package com.uneedbot.excommonmodule.model

import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by kimjinmo on 2019.07.23
 */
data class MenuSideGroup(
        var sideGroupUid: Int = 0,
        var storeUid: Int = 0,
        var groupName: String,
        var isNecessary: Int = 0,
        var maxGroupItem: Int = 1,
        @SerializedName("sideOptions") var sideOptions: ArrayList<Menu> = ArrayList()
) : BaseData()

