package com.uneedbot.excommonmodule.ViewModel.Socket

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.neovisionaries.ws.client.*
import com.uneedbot.excommonmodule.model.Order
import com.uneedbot.excommonmodule.utils.LogUtil
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import java.net.URI
import java.util.concurrent.TimeUnit


/**
 * created by jmkim. 2019.07.22
 * WS를 다루기위한 ViewModel
 *
 * **/
class SocketViewModel : ViewModel() {

    val gson = Gson()

    companion object {
        val COMMAND_REGISTER_KDS = "register-kds"
        val COMMAND_KDS_UPDATE = "kds-update"
        val COMMAND_KDS_UPDATED = "kds-updated"
        val COMMAND_KDS_ADDED = "kds-added"
        val COMMAND_KDS_ORDER = "kds-order"
    }

   public var state: MutableLiveData<STATE>? = MutableLiveData()
    var compositeDisposable = CompositeDisposable()


    fun initState()
    {
        this.state?.postValue(SocketViewModel.STATE.CLOSED)

    }
    enum class STATE {
        OPENED, CONNECTED, OPENING, CLOSED
    }

    ///각종 publishSubject
    var emitPing = PublishSubject.create<Boolean>()         //ping pong 처리
    var messageSubject: PublishSubject<String> = PublishSubject.create()
    var addOrder: PublishSubject<String> = PublishSubject.create()
    var initOrder: PublishSubject<String> = PublishSubject.create()
    var updatedOrder: PublishSubject<String> = PublishSubject.create()


    var ws: WebSocket? = null

    /**
     * ws 커넥트.
     * */
    fun connect(url: String): SocketViewModel {
        this.state?.postValue(STATE.OPENING)
        this.compositeDisposable.dispose()
        this.compositeDisposable = CompositeDisposable()

        ///ping-pong 5초단위로 처리
        this.compositeDisposable.add(
                this.emitPing
                        .sample(5000, TimeUnit.MILLISECONDS, true)
                        .subscribe({
                            LogUtil.d("핑퐁 처리 중 : " + it)
                            this.sendMessage("2")
                        }, {

                        })
        )

        val transURL = "ws://" + url + ":9626/socket.io/?EIO=3&transport=websocket"
        LogUtil.d("socket url : " + transURL)
        this.ws = WebSocketFactory()
                .setConnectionTimeout(10000)
                .createSocket(URI.create(transURL))
                .addListener(object : WebSocketListener {
                    override fun handleCallbackError(websocket: WebSocket?, cause: Throwable?) {

                        LogUtil.d("socket handleCallbackError : " + cause)
                        cause?.printStackTrace()
                    }

                    override fun onFrame(websocket: WebSocket?, frame: WebSocketFrame?) {

                        LogUtil.d("socket onFrame : " + frame.toString())
                    }

                    override fun onThreadCreated(websocket: WebSocket?, threadType: ThreadType?, thread: Thread?) {
                        LogUtil.d("socket onThreadCreated : " + thread.toString())

                    }

                    override fun onThreadStarted(websocket: WebSocket?, threadType: ThreadType?, thread: Thread?) {
                        LogUtil.d("socket onThreadStarted : " + thread.toString())

                    }

                    override fun onStateChanged(websocket: WebSocket?, newState: WebSocketState?) {
                        LogUtil.d("socket onStateChanged : " + newState.toString())

                    }

                    override fun onTextMessageError(websocket: WebSocket?, cause: WebSocketException?, data: ByteArray?) {
                        LogUtil.d("socket onTextMessageError : " + cause)
                        cause?.printStackTrace()


                    }

                    override fun onTextFrame(websocket: WebSocket?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onTextFrame : " + frame.toString())
                        frame?.payloadText.let {
                            LogUtil.d("socket payloadText : " + it)

                            if (it.equals("3")) {
                                emitPing.onNext(true)
                            } else if (it!!.startsWith("42[\"" + COMMAND_KDS_ADDED)) {      ///새로운 메뉴 들어옴
                                addOrder.onNext(parseOrder(it))
                            } else if (it!!.startsWith("42[\"" + COMMAND_KDS_UPDATED)) {      ///새로운 메뉴 들어옴
                                updatedOrder.onNext(parseOrder(it))
                            } else if (it!!.startsWith("42[\"" + COMMAND_KDS_ORDER)) {      ///메뉴 가져옴

                                initOrder.onNext(parseOrder_kds_order(it))

                            }

                        }


                    }

                    override fun onUnexpectedError(websocket: WebSocket?, cause: WebSocketException?) {
                        LogUtil.d("socket onUnexpectedError : " + cause)
                        cause?.printStackTrace()

                    }

                    override fun onConnectError(websocket: WebSocket?, cause: WebSocketException?) {
                        LogUtil.d("socket onConnectError : " + cause)
                        cause?.printStackTrace()

                    }

                    override fun onSendError(websocket: WebSocket?, cause: WebSocketException?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onSendError : " + cause)
                        cause?.printStackTrace()

                    }

                    override fun onFrameUnsent(websocket: WebSocket?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onFrameUnsent : " + frame.toString())

                    }

                    override fun onDisconnected(websocket: WebSocket?, serverCloseFrame: WebSocketFrame?, clientCloseFrame: WebSocketFrame?, closedByServer: Boolean) {
                        LogUtil.d("socket onDisconnected : " + serverCloseFrame?.closeReason)
                        state?.postValue(STATE.CLOSED)

                        /**
                         * disconnected 되었을때 사용중이던 publishSubject를 complete 해줘야한다.
                         * */
                        messageSubject.onComplete()
                    }

                    override fun onSendingFrame(websocket: WebSocket?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onSendingFrame : " + frame.toString())

                    }

                    override fun onBinaryFrame(websocket: WebSocket?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onBinaryFrame : " + frame.toString())

                    }

                    override fun onPingFrame(websocket: WebSocket?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onPingFrame : " + frame.toString())

                    }

                    override fun onSendingHandshake(websocket: WebSocket?, requestLine: String?, headers: MutableList<Array<String>>?) {
                        LogUtil.d("socket onSendingHandshake : " + requestLine.toString())

                    }

                    override fun onTextMessage(websocket: WebSocket?, text: String?) {
                        if (text != null)
                            messageSubject.onNext(text)

                    }

                    override fun onTextMessage(websocket: WebSocket?, data: ByteArray?) {
                        LogUtil.d("socket onTextMessage : " + data.toString())

                    }

                    override fun onFrameError(websocket: WebSocket?, cause: WebSocketException?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onFrameError : " + cause)
                        cause?.printStackTrace()

                    }

                    override fun onCloseFrame(websocket: WebSocket?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onCloseFrame : " + frame.toString())

                    }

                    override fun onBinaryMessage(websocket: WebSocket?, binary: ByteArray?) {
                        LogUtil.d("socket onBinaryMessage : " + binary.toString())

                    }

                    override fun onContinuationFrame(websocket: WebSocket?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onContinuationFrame : " + frame.toString())

                    }

                    override fun onConnected(websocket: WebSocket?, headers: MutableMap<String, MutableList<String>>?) {
                        state?.postValue(STATE.CONNECTED)
                        LogUtil.d("socket onConnected : " + headers.toString())


                    }

                    override fun onFrameSent(websocket: WebSocket?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onFrameSent : " + frame.toString())

                    }

                    override fun onThreadStopping(websocket: WebSocket?, threadType: ThreadType?, thread: Thread?) {
                        LogUtil.d("socket onThreadStopping : " + thread.toString())

                    }

                    override fun onError(websocket: WebSocket?, cause: WebSocketException?) {
                        LogUtil.d("socket onError : " + cause)
                        cause?.printStackTrace()

                    }

                    override fun onMessageDecompressionError(websocket: WebSocket?, cause: WebSocketException?, compressed: ByteArray?) {
                        LogUtil.d("socket onMessageDecompressionError : " + cause)
                        cause?.printStackTrace()

                    }

                    override fun onPongFrame(websocket: WebSocket?, frame: WebSocketFrame?) {
                        LogUtil.d("socket onPongFrame : " + frame.toString())

                    }

                    override fun onMessageError(websocket: WebSocket?, cause: WebSocketException?, frames: MutableList<WebSocketFrame>?) {
                        LogUtil.d("socket onMessageError : " + cause)
                        cause?.printStackTrace()

                    }

                })
        this.ws?.connect()
        return this
    }

    /**
     * message내에서 주문목록을 파싱하기위한 코드
     * */
    fun parseOrder(input: String): String {
        return input.substring(input.indexOfFirst { it == ',' } + 1, input.indexOfLast { it == ']' })
    }

    /**
     * message내에서 주문목록을 파싱하기위한 코드
     * */
    fun parseOrder_kds_order(input: String): String {
        return input.substring(input.indexOfFirst { it == ',' } + 1, input.indexOfLast { it == ']' } )
    }


    fun sendMessage(message: String) {
        LogUtil.d("socket sendMessage : " + message)

        this.ws?.sendText(message)
    }

    fun commandGetList(state: Int) {
        var command = (if (state == 1) "426" else if (state == 0) "427" else "428") + "[\"" + COMMAND_KDS_ORDER + "\", {\"state\": " + state + "}]"

        sendMessage(command)

    }

    fun commandRegisterKDS(deviceName: String) {
        sendMessage("420[\"" + COMMAND_REGISTER_KDS + "\",{\"ipAddr\":\"192.168.0.11\",\"deviceName\":\"" + deviceName + "\"}]")

    }

    fun commandKDSUpdate(newState: Int, order: Order) {
        sendMessage("421[\"" + COMMAND_KDS_UPDATE + "\", {\"state\": " + newState + ", \"orderItemKey\": \"" + order.orderItemKey + "\"}]")
    }


    fun disconnect() {

        if (this.ws != null && ws!!.isOpen)
            this.ws!!.disconnect()
    }

    override fun onCleared() {
        super.onCleared()
        this.disconnect()

    }

}
