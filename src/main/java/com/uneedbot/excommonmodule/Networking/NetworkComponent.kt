package com.uneedbot.excommonmodule.Networking

import dagger.Component
import javax.inject.Singleton

interface NetworkComponent {
    fun inject(networkUtil: NetworkUtil)
}