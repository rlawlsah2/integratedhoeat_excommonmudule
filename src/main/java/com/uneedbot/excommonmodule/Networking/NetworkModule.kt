package com.uneedbot.excommonmodule.Networking

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Headers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

//@Module
abstract open class NetworkModule {


    abstract fun getHeaders() : HashMap<String,String>
    abstract fun readTimeout() : Long
    abstract fun connectTimeout() : Long
    abstract fun baseUrl() : String

    open fun setRetrofit() : Retrofit {
        val logging = HttpLoggingInterceptor().also {
            it.level = HttpLoggingInterceptor.Level.BODY
        }


        val okHttpClient = OkHttpClient.Builder().addInterceptor {
            chain ->
            var original = chain.request()

            var request = original.newBuilder()

            val addHeaders = getHeaders()

            for(key in addHeaders.keys) addHeaders.get(key)?.let { request.addHeader(key, it) }


            val response = chain.proceed(request.build()).also {
            }

            response
        }.also {
            it.readTimeout(readTimeout(), TimeUnit.SECONDS)
            it.connectTimeout(connectTimeout(), TimeUnit.SECONDS)
            it.addInterceptor(logging)
        }.build()

        val gson = GsonBuilder().setLenient().create()

        return Retrofit.Builder()
                .baseUrl(baseUrl())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

    }


//    @Provides
//    @Singleton
//    fun providesNetworkService(retrofit: Retrofit) : NetworkService{
//        return retrofit.create(NetworkService::class.java)
//    }


//    @Provides
//    @Singleton
//    fun providesService(networkService: NetworkService) : Service{
//        return Service(networkService)
//    }



}