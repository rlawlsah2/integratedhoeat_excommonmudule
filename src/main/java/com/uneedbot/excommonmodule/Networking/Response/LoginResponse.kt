package com.uneedbot.excommonmodule.Networking.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.uneedbot.excommonmodule.model.LoginData

/**
 * Created by kimjinmo on 2016. 11. 10..
 */

class LoginResponse : BaseResponse() {


    @SerializedName("result")
    @Expose
    var result : LoginData? = null
}
