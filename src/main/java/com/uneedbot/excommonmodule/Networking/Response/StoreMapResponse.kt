package com.uneedbot.excommonmodule.Networking.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.uneedbot.excommonmodule.model.*

/**
 * Created by kimjinmo on 2019.08.30
 * 매장 내 테이블 정보 조회
 */

class StoreMapResponse : BaseResponse() {


    @SerializedName("result")
    @Expose
    var result: TableList? = null

    class TableList : BaseData() {
        @SerializedName("tables")
        @Expose
        var tables: HashMap<String, Table>? = null
    }

}
