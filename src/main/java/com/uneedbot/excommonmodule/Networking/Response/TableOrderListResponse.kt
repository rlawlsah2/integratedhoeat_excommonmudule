package com.uneedbot.excommonmodule.Networking.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.uneedbot.excommonmodule.model.*

/**
 * Created by kimjinmo on 2019.09.08
 *
 */

class TableOrderListResponse : BaseResponse() {


    @SerializedName("result")
    @Expose
    var result: TableOrderInfo? = null


}
