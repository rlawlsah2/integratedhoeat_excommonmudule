package com.uneedbot.excommonmodule.Networking.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.uneedbot.excommonmodule.model.BaseData
import com.uneedbot.excommonmodule.model.Category
import com.uneedbot.excommonmodule.model.LoginData
import com.uneedbot.excommonmodule.model.Menu

/**
 * Created by kimjinmo on 2019.09.08
 *
 */

class OrderResultResponse : BaseResponse() {



    @SerializedName("orderId")
    @Expose
    var orderId: String? = null


}
