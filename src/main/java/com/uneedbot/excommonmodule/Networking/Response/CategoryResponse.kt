package com.uneedbot.excommonmodule.Networking.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.uneedbot.excommonmodule.model.BaseData
import com.uneedbot.excommonmodule.model.Category
import com.uneedbot.excommonmodule.model.LoginData

/**
 * Created by kimjinmo on 2016. 11. 10..
 */

class CategoryResponse : BaseResponse() {


    @SerializedName("result")
    @Expose
    var result : CategoryList? = null



    class CategoryList : BaseData(){
        @SerializedName("categories")
        @Expose
        var categories : ArrayList<Category>? = null
    }

}
