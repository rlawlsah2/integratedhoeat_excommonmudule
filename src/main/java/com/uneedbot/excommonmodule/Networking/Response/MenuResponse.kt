package com.uneedbot.excommonmodule.Networking.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.uneedbot.excommonmodule.model.BaseData
import com.uneedbot.excommonmodule.model.Category
import com.uneedbot.excommonmodule.model.LoginData
import com.uneedbot.excommonmodule.model.Menu

/**
 * Created by kimjinmo on 2019.08.12
 */

class MenuResponse : BaseResponse() {


    @SerializedName("result")
    @Expose
    var result: MenuData? = null

    class MenuData : BaseData() {
        @SerializedName("menus")
        @Expose
        var menus: ArrayList<Menu>? = null
    }

}
