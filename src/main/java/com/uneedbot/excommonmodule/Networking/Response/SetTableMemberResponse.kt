package com.uneedbot.excommonmodule.Networking.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.uneedbot.excommonmodule.model.*

/**
 * Created by kimjinmo on 2019.09.08
 *
 */

class SetTableMemberResponse : BaseResponse() {


    @SerializedName("result" )
    @Expose
    var result: TableInfo? = null

    class TableInfo()
    {

        @SerializedName("table" )
        @Expose
        var table: TableOrderInfo? = null
    }


}
