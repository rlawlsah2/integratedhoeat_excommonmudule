package com.uneedbot.excommonmodule.Networking.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.uneedbot.excommonmodule.model.StatusData

import io.reactivex.annotations.Nullable

/**
 * Created by kimjinmo on 2016. 11. 8..
 */

open class BaseResponse {

    @Expose
    @Nullable
    @SerializedName("status")
    lateinit var status: StatusData


    @SerializedName("errCode")
    @Nullable
    @Expose
    var errCode: String? = null
}
