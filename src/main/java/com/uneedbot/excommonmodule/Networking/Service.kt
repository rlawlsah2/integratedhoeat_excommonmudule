package com.uneedbot.excommonmodule.Networking

import android.util.Base64
import com.google.gson.Gson
import com.uneedbot.excommonmodule.Networking.Response.*
import com.uneedbot.excommonmodule.model.Notification
import com.uneedbot.excommonmodule.utils.LogUtil
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody


open class Service<T : NetworkService> {
    val networkService: T

    constructor(networkService: T) {
        this.networkService = networkService
    }


    /**
     * token + tableNo base 로그인
     * **/
    fun getLoginInfo(token: String, tableNo: String): Observable<LoginResponse> {
        var newToken: String? = null

        try {
            newToken = String(Base64.encode("$token:$tableNo".toByteArray(charset("UTF-8")), Base64.NO_WRAP))

        } catch (e: Exception) {
            newToken = null
        }

        newToken = "Basic " + newToken!!
        return this.networkService.Login(
                newToken
        )
    }

    /**
     * 구글 로그인
     * */
    fun loginWithGooleAccount(encodedEmail: String): Observable<LoginResponse> {
        var newToken: String? = null

        try {
            newToken = String(Base64.encode("$encodedEmail".toByteArray(charset("UTF-8")), Base64.NO_WRAP))

        } catch (e: Exception) {
            newToken = ""
        }

        return this.networkService.LoginWithGooleAccount(
                newToken!!
        )
    }


    ////점원용 로그인
    fun loginWithStaffIdPw(branchId: String, password: String): Observable<LoginResponse> {

        val requestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("branchId", branchId)
                .addFormDataPart("password", password)
                .build()

        return this.networkService.loginWithStaffIdPw(
                requestBody
        )

    }

    /**
     * 대분류 카테고리를 가져온다.
     * */
    fun getRootCategory(storeBranchUid: String): Observable<CategoryResponse> {
        return this.networkService.getRootCategory(
                storeBranchUid
        )
    }

    /**
     * 하위 카테고리 불러오기
     */
    fun getSubCategoryList(categoryId: String, storeBranchUid: String): Observable<CategoryResponse> {
        return this.networkService.getSubCategory(categoryId,
                storeBranchUid
        )
    }

    /**
     * 메뉴 목록 불러오기
     */
    fun getMenuList(categoryId: String, storeBranchUid: String): Observable<MenuResponse> {
        return this.networkService.getMenuList(categoryId,
                storeBranchUid
        )
    }

    /**
     * 테이블 메뉴 가져오기
     */
    fun getTableInfo(url: String): Observable<TableOrderListResponse> {
        LogUtil.d("posUrl : " + url)
        return this.networkService.getTableInfo(url)
    }


    /**
     * 메뉴 주문하기
     */
    fun order(url: String, orderJson: String): Observable<OrderResultResponse> {
        val requestBody = RequestBody.create("application/json; charset=utf-8".toMediaTypeOrNull(), orderJson)
        return this.networkService.order(url, requestBody)
    }

    /**
     * 메뉴 주문하기
     */
    fun setTableMembers(url: String, members: String): Observable<SetTableMemberResponse> {
        val requestBody = RequestBody.create("application/json; charset=utf-8".toMediaTypeOrNull(), members)
        return this.networkService.setTableMembers(url, requestBody)
    }


    /**
     * 매장 내 모든 테이블 정보 조회
     * */
    fun getAllMapInfo(url: String): Observable<StoreMapResponse> {

        return this.networkService.getAllMapInfo(url)
    }


    /**
     * 서비스 요청하기
     */
    fun request(url: String, notification: Notification): Observable<String> {
        val gson = Gson()
        val requestBody = RequestBody.create("application/json; charset=utf-8".toMediaTypeOrNull(), gson.toJson(notification))
        return this.networkService.postRequestForQueue(
                url,
                requestBody
        )
    }


}