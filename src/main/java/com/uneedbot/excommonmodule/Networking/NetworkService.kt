package com.uneedbot.excommonmodule.Networking

import androidx.annotation.Keep
import com.uneedbot.excommonmodule.Networking.Response.*
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.*
import java.util.*


open interface NetworkService {

    @Keep
    @GET("admin/api/login")
    fun Login(
            @Header("Authorization") authorization: String
    ): Observable<LoginResponse>


    /**
     * 구글계정 로그인을 연동한 로그인 처리.
     * @param xCertKey 이메알 주소를 Base64로 인코딩
     * */
    @Keep
    @GET("admin/api/certification")
    fun LoginWithGooleAccount(
            @Header("X-CERT-KEY") xCertKey: String): Observable<LoginResponse>

    /**
     * 스태프 계정으로 로그인 처리.
     * */
    @Keep
    @POST("admin/api/login?isStaffLogin=true")
    fun loginWithStaffIdPw(
            @Body array: RequestBody): Observable<LoginResponse>


    /**
     * 해당 매장의 대분류 카테고리 가져오기
     * */
    @Keep
    @GET("admin/api/categories")
    fun getRootCategory(
            @Query("storeBranchUid") storeBranchUid: String
    ): Observable<CategoryResponse>

    /**
     * 테이블 인원 셋팅
     * */
    @Keep
    @POST  //("data/v2/tables/{tableNo}")
    fun setTableMembers(
            @Url url: String,
//            @Path("tableNo") tableNo: String,
            @Body array: RequestBody): Observable<SetTableMemberResponse>


    /**
     * 해당 매장의 하위 카테고리 가져오기
     * */
    @Keep
    @GET("admin/api/categories/{categoryId}")
    fun getSubCategory(
            @Path("categoryId") categoryId: String,
            @Query("storeBranchUid") storeBranchUid: String
    ): Observable<CategoryResponse>


    /**
     * 메뉴 가져오기
     * */
    @Keep
    @GET("admin/api/categories/{categoryId}/menus")
    fun getMenuList(
            @Path("categoryId") categoryId: String,
            @Query("storeBranchUid") storeBranchUid: String
    ): Observable<MenuResponse>


    /**
     * 테이블 주문내역 가져오기
     * */
    @Keep
    @GET
    fun getTableInfo(@Url posUrl: String
    ): Observable<TableOrderListResponse>


    /**
     * 큐에 요청할때.
     * */
    @Keep
    @GET
    @JvmSuppressWildcards
    fun <T> getRequestForQueue(@Url posUrl: String, returnType: T
    ): Observable<T>

    /**
     * 큐에 요청할때.
     * */
    @Keep
    @POST
    @JvmSuppressWildcards
    fun postRequestForQueue(@Url posUrl: String, @Body array: RequestBody
    ): Observable<String>


    ////큐. 주문넣기
    @Keep
    @POST
    fun order(
            @Url url: String,
            @Body array: RequestBody
    ): Observable<OrderResultResponse>


    @Keep
    @GET
    fun getAllMapInfo(@Url url: String
    ): Observable<StoreMapResponse>

}

