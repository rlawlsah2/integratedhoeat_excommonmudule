package com.uneedbot.excommonmodule.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import com.uneedbot.excommonmodule.model.Category
import com.uneedbot.excommonmodule.model.Menu
import com.uneedbot.excommonmodule.utils.LogUtil
import com.uneedbot.excommonmodule.utils.OrderInterface
import com.uneedbot.excommonmodule.views.BaseCartListAdapter
import java.lang.StringBuilder

/**
 * Created by kimjinmo on 2019.10.16
 * CartView에 기본적으로 필요한 기능 작업.
 * 실제 사용하는 view는 이 클래스를 상속받아 구현할것.
 */

open abstract class BaseCartView<T : ViewDataBinding> : ConstraintLayout {

    var cartInterface: BaseCartListAdapter.CartInterface? = null
    var orderInterface: OrderInterface? = null
    lateinit var binding: T
    var mContext: Context

    var cartList: ObservableArrayList<Pair<Category, Menu>>? = null
    var totalPrice = 0

    constructor(context: Context) : super(context) {
        this.mContext = context
        registerHandler()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        this.mContext = context
        registerHandler()

    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        this.mContext = context
        registerHandler()
    }

    open fun registerHandler() {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(context), this.getLayoutId(), this, true)

    }

    abstract fun getLayoutId(): Int


    fun goBill() {

    }

    fun goOrder() {

        this.orderInterface?.goOrder()


    }

    open fun connectCartList(cartList: ObservableArrayList<Pair<Category, Menu>>) {

        this.cartList = cartList

        cartList.addOnListChangedCallback(
                object : ObservableList.OnListChangedCallback<ObservableArrayList<Pair<Category, Menu>>>() {
                    override fun onChanged(list: ObservableArrayList<Pair<Category, Menu>>?) {

                        LogUtil.d("변경된게 느껴지나 onChanged: " + cartList?.size)
                        updateCartList()
                    }

                    override fun onItemRangeRemoved(list: ObservableArrayList<Pair<Category, Menu>>?, positionStart: Int, itemCount: Int) {
                        LogUtil.d("변경된게 느껴지나 onItemRangeRemoved: " + cartList?.size)
                        updateCartList()
                    }

                    override fun onItemRangeMoved(list: ObservableArrayList<Pair<Category, Menu>>?, fromPosition: Int, toPosition: Int, itemCount: Int) {
                        LogUtil.d("변경된게 느껴지나 onItemRangeMoved: " + cartList?.size)
                        updateCartList()
                    }

                    override fun onItemRangeInserted(list: ObservableArrayList<Pair<Category, Menu>>?, positionStart: Int, itemCount: Int) {
                        LogUtil.d("변경된게 느껴지나 onItemRangeInserted: " + cartList?.size)
                        updateCartList()
                    }

                    override fun onItemRangeChanged(list: ObservableArrayList<Pair<Category, Menu>>?, positionStart: Int, itemCount: Int) {
                        LogUtil.d("변경된게 느껴지나 onItemRangeChanged: " + cartList?.size)
                        updateCartList()
                    }

                }
        )
    }

    open fun updateCartList() {
        this.updateTotalPrice()

    }

    open fun updateTotalPrice() {

        this.totalPrice = 0

        this.cartList?.iterator()?.let {
            while (it.hasNext()) {
                it.next()?.second?.let { menu ->
                    if (menu?.isSet == 1) {
                        //세트메뉴
                        var totalPrice = 0
                        var optionsBuilder = StringBuilder()
                        LogUtil.d("장바구니 옵션 확인 : " + menu?.selectedOption?.toString())

                        menu.selectedOption?.keys?.onEach { key ->
                            menu.selectedOption?.get(key)?.onEach { option ->
                                totalPrice += (option.price * option.quantity)
                                optionsBuilder.append(" -").append(option.foodName).append(" (").append(option.quantity).append(")\n")
                            }
                        }
                        this.totalPrice += ((totalPrice + menu.price) * menu.quantity)

                    } else {
                        this.totalPrice += (menu.price * menu.quantity)
                    }
                }
            }
        }


    }

}
