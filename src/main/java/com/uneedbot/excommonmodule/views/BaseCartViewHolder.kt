package com.uneedbot.excommonmodule.views

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.uneedbot.excommonmodule.model.Category
import com.uneedbot.excommonmodule.model.Menu


/**
 * Created by kimjinmo on 2019.10.20
 * 장바구니 베이스 코드의 ViewHolder
 */

abstract open class BaseCartViewHolder<T : ViewDataBinding>(itemView: View)
    : RecyclerView.ViewHolder(itemView) {


    var binding: T? = null

//    init {
//        binding = DataBindingUtil.bind(itemView)
//    }


    abstract fun setCartItem(position: Int, category: Category, menu: Menu)

//    fun setCartItem(position: Int, category: Category?, menu: Menu?) {
//
//        binding?.index = position
//        binding?.category = category
//        binding?.menu = menu
//
//        if (menu?.isSet == 1) {
//            //세트메뉴
//            var totalPrice = 0
//            var optionsBuilder = StringBuilder()
//            LogUtil.d("장바구니 옵션 확인 : " + menu?.selectedOption?.toString())
//
//            menu.selectedOption?.keys?.onEach { key ->
//                menu.selectedOption?.get(key)?.onEach { option ->
//                    totalPrice += (option.price * option.quantity)
//                    optionsBuilder.append(" -").append(option.foodName).append(" (").append(option.quantity).append(")\n")
//                }
//            }
//
//            binding?.price = "" + Utils.setComma(totalPrice) + ""
//            binding?.options = optionsBuilder.toString()
//
//        } else {
//            binding?.price = "" + Utils.setComma(menu?.price ?: 0) + ""
//        }
//
//
//    }

}
