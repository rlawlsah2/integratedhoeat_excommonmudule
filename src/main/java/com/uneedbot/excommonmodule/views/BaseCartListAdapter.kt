package com.uneedbot.excommonmodule.views

import android.view.ViewGroup
import androidx.databinding.ObservableArrayList
import androidx.recyclerview.widget.RecyclerView
import com.uneedbot.excommonmodule.model.Category
import com.uneedbot.excommonmodule.model.Menu


/**
 * Created by kimjinmo on 2019.10.20
 * 장바구니 베이스 코드
 */

abstract open class BaseCartListAdapter<T : BaseCartViewHolder<*>> : RecyclerView.Adapter<T>() {

    var mListData: ObservableArrayList<Pair<Category, Menu>>? = ObservableArrayList()
    open var removeListener: CartInterface? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): T {
        return this.getCreatedViewHolder(parent, viewType)
    }

    abstract fun getCreatedViewHolder(parent: ViewGroup, viewType: Int): T

    override fun getItemId(position: Int): Long {
        this.mListData?.get(position)?.let {
            return it.toString().hashCode().toLong()
        }

        return 0
    }

    override fun onBindViewHolder(holder: T, position: Int) {

    }


    /**
     * 삭제 누른경우
     * **/
    open fun cancelItem(index: Int, category: Category?, menu: Menu) {

        this.mListData?.get(index)?.let { removeData ->
            //            this.mListData?.remove(removeData)
            this.removeListener?.let {
                it.removeCart(removeData)
            }
            this.notifyDataSetChanged()
        }
    }


    /**
     * 수량 변경
     * **/
    open fun changeQuantity(isPlus: Boolean, position: Int) {
        this.mListData?.get(position)?.let { menu ->
            this.removeListener?.let {
                it.changeQuantity(isPlus, menu)
            }
            this.notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return this.mListData?.size ?: 0
    }

//    class ViewHolder(itemView: View, adapter: BaseCartListAdapter) : RecyclerView.ViewHolder(itemView) {
//
//        internal var binding: KotlinAdapterType1CartListBinding? = null
//
//        init {
//            binding = DataBindingUtil.bind(itemView)
//            binding?.adapter = adapter
//
//        }
//
//        fun setCartItem(position: Int, category: Category?, menu: Menu?) {
//
//            binding?.index = position
//            binding?.category = category
//            binding?.menu = menu
//
//            if (menu?.isSet == 1) {
//                //세트메뉴
//                var totalPrice = 0
//                var optionsBuilder = StringBuilder()
//                LogUtil.d("장바구니 옵션 확인 : " + menu?.selectedOption?.toString())
//
//                menu.selectedOption?.keys?.onEach { key ->
//                    menu.selectedOption?.get(key)?.onEach { option ->
//                        totalPrice += (option.price * option.quantity)
//                        optionsBuilder.append(" -").append(option.foodName).append(" (").append(option.quantity).append(")\n")
//                    }
//                }
//
//                binding?.price = "" + Utils.setComma(totalPrice) + ""
//                binding?.options = optionsBuilder.toString()
//
//            } else {
//                binding?.price = "" + Utils.setComma(menu?.price?:0) + ""
//            }
//
//
//        }
//    }


    /**
     * 장바구니 담기
     * */
    interface CartInterface {
        fun addCart(menu: Pair<Category, Menu>)
        fun addCartAsSetMenu(menu: Pair<Category, Menu>)
        fun removeCart(menu: Pair<Category, Menu>)
        fun changeQuantity(isPlus: Boolean, menu: Pair<Category, Menu>)
        fun getAllCartList(): ArrayList<Pair<Category, Menu>>?
    }

}
