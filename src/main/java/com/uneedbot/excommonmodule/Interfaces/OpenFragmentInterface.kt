package com.uneedbot.excommonmodule.Interfaces

import android.os.Bundle

interface OpenFragmentInterface {
    fun openFragment(fragment: String, bundle: Bundle?)
}