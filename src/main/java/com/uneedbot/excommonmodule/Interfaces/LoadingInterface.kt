package com.uneedbot.excommonmodule.Interfaces

interface LoadingInterface{
    fun showLoading(message : String? = null)
    fun hideLoading()
}