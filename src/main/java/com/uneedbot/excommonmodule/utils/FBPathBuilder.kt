package com.uneedbot.excommonmodule.utils

/**
 * Created by kimjinmo on 2020.02
 */
class FBPathBuilder private constructor() {
    private val builder: StringBuilder
    fun init(): FBPathBuilder {
        builder.setLength(0)
        builder.append("/")
        return this
    }

    fun set(path: String?): FBPathBuilder {
        builder.append(path)
        builder.append("/")
        return this
    }

    fun complete(): String {
        return builder.toString()
    }

    companion object {
        var instance: FBPathBuilder? = null
            get() {
                if (field == null) field = FBPathBuilder()
                return field
            }
            private set
    }

    init {
        builder = StringBuilder()
    }
}