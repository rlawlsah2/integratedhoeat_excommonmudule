package com.uneedbot.excommonmodule.utils

import android.content.Context
import com.uneedbot.excommonmodule.model.LoginData

/**
 * Created by kimjinmo on 2017. 9. 20..
 * Refactor 2019.07.14
 */

class LoginUtil {
    constructor()

    companion object {
        private var INSTANCE: LoginUtil? = null

        fun getInstance(): LoginUtil? {
            if (INSTANCE == null) {
                INSTANCE = LoginUtil()
            }
            return INSTANCE
        }
    }

    var loginInfo: LoginData? = null       //로그인 성공했을때 초기화
    public var lastLoginStoreName: String? = null     //로그인 리커버리 할경우, 이전에 성공한 로그인 정보(매장명)를 가져옴
    public var lastLoginToken: String? = null     //로그인 리커버리 할경우, 이전에 성공한 로그인 정보(토큰)를 가져옴
    public var lastLoginTableNo: String? = null   //로그인 리커버리 할경우, 이전에 성공한 로그인 정보(테이블번호)를 가져옴


    /**
     * 테이블 번호
     * */
    public fun setLastLoginTableNo(value: String, context: Context) {
        this.lastLoginTableNo = value
        //pref 저장
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_TABLENO, value)
    }

    /**
     *
     * */
    public fun getLastLoginTableNo(context: Context): String? {
        if (this.lastLoginTableNo == null) {
            this.lastLoginTableNo = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_TABLENO)
        }

        return this.lastLoginTableNo
    }


    /**
     * 로그인 액세스 토큰
     * */
    public fun setLastLoginToken(value: String, context: Context) {
        this.lastLoginToken = value
        //pref 저장
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ACCESSTOKEN, value)
    }


    public fun getLastLoginToken(context: Context): String? {
        if (this.lastLoginToken == null) {
            this.lastLoginToken = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_ACCESSTOKEN)
        }

        return this.lastLoginToken
    }


    /**
     * 로그인 매장 정보
     *
     * */
    public fun setLastLoginStoreName(value: String, context: Context) {
        this.lastLoginStoreName = value
        //pref 저장
        PreferenceUtils.addStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_STOREBRANCHUID, value)
    }


    public fun getLastLoginStoreName(context: Context): String? {
        if (this.lastLoginStoreName == null) {
            this.lastLoginStoreName = PreferenceUtils.getStringValuePref(context, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_STOREBRANCHUID)
        }
        return this.lastLoginStoreName
    }

    var googleAccessToken: String? = null   //구글 로그인용 액세스 토큰
    var googleEmail: String? = null   //구글 로그인용 이메일

}