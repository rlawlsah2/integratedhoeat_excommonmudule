package com.uneedbot.excommonmodule.utils

import android.content.Context
import android.content.SharedPreferences
import android.os.Environment

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.IOException


/*
 *   Preference 기능에 대해 null일 경우에 대해 처리한 클래스
 *
 * */

object PreferenceUtils {
    fun getPreferences(context: Context?, prefName: String?): SharedPreferences? {
        var preference: SharedPreferences? = null
        if (context == null || prefName == null || "" == prefName) {
            return null
        }
        preference = context.getSharedPreferences(prefName, 0)
        return preference
    }

    fun addStringValuePref(context: Context, prefName: String, key: String, value: String?) {
        val pref = getPreferences(context, prefName)
        if (pref != null) {
            val edit = pref.edit()
            value.let {
                edit.putString(key, it)
            }
            edit.commit()
        }
    }

    fun addBooleanValuePref(context: Context, prefName: String, key: String, value: Boolean) {
        val pref = getPreferences(context, prefName)
        if (pref != null) {
            val edit = pref.edit()
            edit.putBoolean(key, value)
            edit.commit()
        }
    }

    fun addFloatValuePref(context: Context, prefName: String, key: String, value: Float) {
        val pref = getPreferences(context, prefName)
        if (pref != null) {
            val edit = pref.edit()
            edit.putFloat(key, value)
            edit.commit()
        }
    }

    fun addLongValuePref(context: Context, prefName: String, key: String, value: Long) {
        val pref = getPreferences(context, prefName)
        if (pref != null) {
            val edit = pref.edit()
            edit.putLong(key, value)
            edit.commit()
        }
    }

    fun addIntValuePref(context: Context, prefName: String, key: String, value: Int) {
        val pref = getPreferences(context, prefName)
        if (pref != null) {
            val edit = pref.edit()
            edit.putInt(key, value)
            edit.commit()
        }
    }

    fun getStringValuePref(context: Context, prefName: String, key: String): String? {
        val pref = getPreferences(context, prefName)
        return pref?.getString(key, null)
    }

    fun getBooleanValuePref(context: Context, prefName: String, key: String): Boolean {
        val pref = getPreferences(context, prefName)
        return pref?.getBoolean(key, false) ?: false
    }

    fun getFloatValuePref(context: Context, prefName: String, key: String): Float {
        val pref = getPreferences(context, prefName)
        return pref?.getFloat(key, 0.0f) ?: 0.0f
    }

    fun getLongValuePref(context: Context, prefName: String, key: String): Long {
        val pref = getPreferences(context, prefName)
        return pref?.getLong(key, 0L) ?: 0L
    }

    fun getIntValuePref(context: Context, prefName: String, key: String): Int {
        val pref = getPreferences(context, prefName)
        return pref?.getInt(key, 0) ?: 0
    }

    fun clearPref(context: Context, prefName: String) {
        val pref = getPreferences(context, prefName)
        if (pref != null) {
            val edit = pref.edit()
            edit.clear()
            edit.commit()
        }
    }

    fun setStringGlobal(context: Context, prefName: String, key: String, value: String) {
        val pref = context.getSharedPreferences(prefName,
                Context.MODE_WORLD_READABLE)
        if (pref != null) {
            val edit = pref.edit()
            edit.putString(key, value)
            edit.commit()
        }
    }


    fun addTableNoPref(context: Context, value: String) {

        val path = Environment.getExternalStorageDirectory().absolutePath + "/"

        //        File file = new File(path);
        //        if(!file.exists())
        //            file.mkdirs();


        val savefile = File(path + "TableNo.txt")
        LogUtil.d("테이블 번호 저장 시작 : " + savefile.path)
        var fw: BufferedWriter? = null

        try {
            fw = BufferedWriter(FileWriter(savefile, false))
            fw.write(value)
            LogUtil.d("테이블 번호 저장 완료 : $fw")

        } catch (e: IOException) {
            LogUtil.d("테이블 번호 저장 실패 : $e")

        }

        // close file.
        if (fw != null) {
            // catch Exception here or throw.
            try {
                fw.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

    }
}

