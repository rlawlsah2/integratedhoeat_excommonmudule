package com.uneedbot.excommonmodule.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageInfo
import android.content.res.Resources
import android.util.Base64
import android.util.TypedValue
import android.view.WindowManager
import com.uneedbot.excommonmodule.model.Category
import com.uneedbot.excommonmodule.model.Menu
import com.uneedbot.excommonmodule.model.MenuSideGroup
import com.uneedbot.excommonmodule.model.Order
import org.json.JSONArray
import org.json.JSONObject

import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Date
import java.util.HashSet
import java.util.Locale
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

/**
 * Created by kimjinmo on 2017. 9. 20..
 */

open class Utils {

    companion object {


        val date: String
            get() {
                val sdf = SimpleDateFormat("yyyy.MM.dd")
                return sdf.format(Date())
            }

        val time: String
            get() {
                val sdf = SimpleDateFormat("aa hh:mm", Locale.KOREA)
                return sdf.format(Date())
            }

        //휴대폰번호 체크
        private val phoneNumberRegex = "^01(?:0|1[6-9])(?:\\d{3}|\\d{4})\\d{4}$"
        private val emailRegex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$"

        fun getDate(format: String): String {
            val sdf = SimpleDateFormat(format)
            return sdf.format(Date())
        }

        fun setVersion(context: Context): String {
            var pi: PackageInfo? = null
            //버전코드 등록
            try {
                pi = context.packageManager.getPackageInfo(context.packageName, 0)
                val versionCode = pi!!.versionCode.toString()
                val versionName = pi.versionName

                return versionName

            } catch (e: Exception) {
                return ""
            }
        }

        fun getVersion(context: Context): String? {
            try {
                var pi: PackageInfo? = null
                pi = context.packageManager.getPackageInfo(context.packageName, 0)
                val versionCode = pi!!.versionCode.toString()
                val versionName = pi.versionName
                return versionName

            } catch (e: Exception) {
                return null
            }
        }

        fun getPixels(unit: Int, size: Float): Float {
            val metrics = Resources.getSystem().displayMetrics
            return TypedValue.applyDimension(unit, size, metrics)
        }

        fun compareTime(time: String): String {
            var diff: Long = 0
            val currentTimeFormat = SimpleDateFormat("yy-MM-dd HH:mm:ss")
            try {
                val inputDate = currentTimeFormat.parse(time)
                val inputTime = inputDate.time

                ///현재시간
                diff = (Date().time - inputTime) / 60000

            } catch (e: ParseException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {

            }

            return diff.toString()


        }

        fun getCurrentTime(simpleDateFormat: String? = "yy-MM-dd HH:mm:ss"): String {
            return SimpleDateFormat(simpleDateFormat).format(Calendar.getInstance().time)
        }

        /**
         * 맨앞에 위치한 공백을 제거
         */
        fun setTitle(data: String?): String? {
            var data = data
            while (data != null && data.startsWith(" ")) {
                data = data.replaceFirst(" ".toRegex(), "")
            }
            return data
        }

        fun convertTime(time: String): String? {
            val currentTimeFormat = SimpleDateFormat("yy-MM-dd HH:mm:ss")
            val newTimeFormat = SimpleDateFormat("MM-dd HH:mm")

            var outputDate: String? = null

            try {
                val inputDate = currentTimeFormat.parse(time)
                outputDate = newTimeFormat.format(inputDate)

            } catch (e: ParseException) {
                e.printStackTrace()
                outputDate = ""
            } catch (e: NullPointerException) {

            }

            return outputDate


        }

        /**
         * dp -> pixel 변환
         *
         * @param context
         * @param dp      dp값
         * return int 픽셀로 변환된 값
         */
        fun dpToPx(context: Context, dp: Int): Int {
            val resources = context.resources

            val metrics = resources.displayMetrics

            val px = dp * (metrics.densityDpi / 160f)

            return px.toInt()
        }

        /**
         * pixel -> dp 변환
         *
         * @param context
         * @param px      px 값
         * return int dp로 변환된 값
         */
        fun pxToDp(context: Context, px: Int): Int {
            val resources = context.resources

            val metrics = resources.displayMetrics

            val dp = px / (metrics.densityDpi / 160f)

            return dp.toInt()

        }

        @JvmStatic
        fun setComma(data: Int): String {
            val result = Integer.parseInt(data.toString() + "")
            return java.text.DecimalFormat("#,###").format(result.toLong())
        }

        @JvmStatic
        fun setComma(data: String?): String? {
            if (data == null)
                return data
            val result = Integer.parseInt(data)
            return java.text.DecimalFormat("#,###").format(result.toLong())
        }

        fun UnixTimeNow(): Long {
            return System.currentTimeMillis()

        }


        fun getWindosWidth(mContext: Context): Int {

            val lp = (mContext as Activity).window.attributes
            val wm = mContext.getApplicationContext().getSystemService(Context.WINDOW_SERVICE) as WindowManager
            return wm.defaultDisplay.width
        }

        fun getWindosHeight(mContext: Context): Int {

            val lp = (mContext as Activity).window.attributes
            val wm = mContext.getApplicationContext().getSystemService(Context.WINDOW_SERVICE) as WindowManager
            return wm.defaultDisplay.height
        }


        fun timeToShowAD(lastTouchTime: Date, interval: Long): Boolean {
            val diffInMs = lastTouchTime.time - Calendar.getInstance().time.time
            val diffInSec = Math.abs(TimeUnit.MILLISECONDS.toSeconds(diffInMs))
            return diffInSec > interval
        }


        // test for . $ # [ ] / and replace them
        fun removeInvalidChars(text: String): String {
            var text = text
            text = text.replace(".", ",")
            text = text.replace("$", "@")
            text = text.replace("#", "*")
            text = text.replace("[", "(")
            text = text.replace("]", ")")
            text = text.replace("/", "backslash")
            return text
        }

        fun makeStackTrace(t: Throwable?): String {
            if (t == null) return ""
            try {
                val bout = ByteArrayOutputStream()
                t.printStackTrace(PrintStream(bout))
                bout.flush()

                return String(bout.toByteArray())
            } catch (ex: Exception) {
                return ""
            }

        }


        fun initOrderID(branchUid: String, selectedTableNo: String): String {
            return "M" + branchUid + "_" + selectedTableNo + "_" + getDate("MMddHHmmss")
        }

        fun checkMobilePhoneNumber(number: String): Boolean {
            return Pattern.matches(phoneNumberRegex, number)
        }

        fun checkEMail(email: String): Boolean {
            return Pattern.matches(emailRegex, email)
        }


        fun removeFirstSpace(input: String): String {
            var input = input
            try {
                while (input.startsWith(" ")) {
                    input = input.replaceFirst(" ".toRegex(), "")
                }
            } catch (e: Exception) {

            }

            return input
        }

        fun getBase64Result(input: String): String? {
            var result: String? = null
            try {
                result = String(Base64.encode(input.toByteArray(charset("UTF-8")), Base64.DEFAULT)).trim { it <= ' ' }

            } catch (e: Exception) {
                result = null
                LogUtil.d("토큰 인코딩 에러 : " + e.message)
            }

            return result
        }

        /**
         * 선택된 옵션 문자열로 변경
         */
        @JvmStatic
        fun getSelectedOptionListAsStr(options: HashMap<String, Order>?): String {
            if (options == null || options.size == 0) {
                return "선택된 옵션없음"
            }

            val str = StringBuilder()
            for (key in options.keys) {
                options.get(key)?.let { item ->
                    str.append("- ").append(item.foodName).append(" (").append(item.quantity).append(") ").append("\n")
                }
            }
            return str.toString()
        }

        /**
         * 선택된 옵션 문자열로 변경
         */
        @JvmStatic
        fun getSelectedOptionListAsStrOneLine(options: HashMap<String, Order>?): String {
            if (options == null || options.size == 0) {
                return "선택된 옵션없음"
            }

            val str = StringBuilder()
            options.keys?.iterator()?.let { iterator ->

                while (iterator?.hasNext()) {
                    (iterator?.next())?.let { key ->
                        options.get(key)?.let {
                            str.append("·").append(it.foodName.trim()).append(" (").append(it.quantity).append(")  ")
//                            if (iterator?.hasNext()) {
//                                str.append("/")
//                            }
                        }
                    }
                }

            }
//            for (key in options.keys) {
//                options.get(key)?.let { item ->
//                    str.append(item.foodName).append(" (").append(item.quantity).append(") ").append("/")
//                }
//            }

            return str.toString()
        }


        /**
         * 선택된 옵션 가격 합산값.(장바구니용)
         */
        @JvmStatic
        fun getSelectedOptionsTotalPrice(options: HashMap<MenuSideGroup?, HashSet<Menu>?>?): Int {
            if (options == null || options.size == 0) {
                return 0
            }
            var totalPrice = 0

            for (item in options.values) {

                item?.iterator()?.let { iterator ->
                    while (iterator.hasNext()) {
                        iterator.next()?.let { menu ->
                            totalPrice += (menu.quantity * menu.price)
                        }
                    }
                }

            }
            return totalPrice
        }

        /**
         * 선택된 옵션 가격 합산값.(주문내역용)
         */
        @JvmStatic
        fun getOrdersSideOptionsTotalPrice(options: HashMap<String, Order>): Int {
            if (options == null || options.size == 0) {
                return 0
            }
            var totalPrice = 0

            for (item in options.values) {
                item?.let { menu ->
                    totalPrice += (menu.quantity * menu.price)
                }
            }
            return totalPrice
        }

        /**
         * 선택된 메뉴의 전체 가격
         */
        @JvmStatic
        fun getTotalPrice(orders: LinkedHashMap<String, Order>?): Int {
            if (orders == null || orders.size == 0) {
                return 0
            }
            var totalPrice = 0

            for (key in orders.keys) {
                orders?.get(key)?.let { order ->

                    ///옵션금액처리
                    totalPrice += getOrderPrice(order)
                }
            }
            return totalPrice
        }

        /**
         * 선택된 메뉴의 전체 가격
         */
        @JvmStatic
        fun getTotalPriceNoCalOptionsPrice(orders: LinkedHashMap<String, Order>?): Int {
            if (orders == null || orders.size == 0) {
                return 0
            }
            var totalPrice = 0

            for (key in orders.keys) {
                orders?.get(key)?.let { order ->

                    ///옵션금액처리
                    totalPrice += getOrderPriceNoCalOptionsPrice(order)
                }
            }
            return totalPrice
        }

        /**
         * 해당 메뉴 총 가격. 할인 코드 포함
         */
        @JvmStatic
        fun getOrderPrice(order: Order?): Int {
            if (order == null) {
                return 0
            }
            var totalPrice = 0
            if (order.sideOptions != null && order.sideOptions.size > 0) {
                totalPrice += order.quantity * ((getOrdersSideOptionsTotalPrice(order?.sideOptions) + order.price) - order.discount)
            } else {
                totalPrice += order.quantity * (order.price - order.discount)
            }
            return totalPrice
        }

        /**
         * 옵션제외 해당 메뉴 총 가격. 할인 코드 포함
         */
        @JvmStatic
        fun getOrderPriceNoCalOptionsPrice(order: Order?): Int {
            if (order == null) {
                return 0
            }
            var totalPrice = order.quantity * (order.price - order.discount)

            return totalPrice
        }


        /**
         * 장바구니에 담긴 내역의 총 가격 합계
         */
        @JvmStatic
        fun getCartTotalPrice(cart: ArrayList<Pair<Category, Menu>>): Int {
            if (cart == null || cart.size == 0) {
                return 0
            }
            var totalPrice = 0
            LogUtil.d("최소 주문 확인중 cart : " + cart.toString())


            cart?.forEach { item ->

                item?.second?.let { menu ->
                    LogUtil.d("최소 주문 확인중 menu : " + menu.toString())


                    var currentOptionPrice = 0 //현재 옵션 총 합산 가격

                    ///옵션 여부 체크
                    if (menu.isSet == 1) {
                        menu?.selectedOption?.forEach { selectedOptionGroup ->
                            LogUtil.d("최소 주문 확인중 selectedOptionGroup : " + selectedOptionGroup.toString())

                            selectedOptionGroup?.value?.forEach { selectedOptionItem ->
                                LogUtil.d("최소 주문 확인중 selectedOptionItem : " + selectedOptionItem.toString())

                                currentOptionPrice += selectedOptionItem.price * selectedOptionItem.quantity
                            }

                        }

                        ///해당 옵션 총 가격 * 수량
                        totalPrice += (currentOptionPrice + menu.price) * menu.quantity
                    } else {
                        ///해당 단품 가격 * 수량
                        totalPrice += menu.price * menu.quantity
                    }
                }
            }

            return totalPrice
        }
    }


}
