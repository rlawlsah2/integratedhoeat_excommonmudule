package com.uneedbot.excommonmodule.utils

import com.uneedbot.excommonmodule.model.Category
import com.uneedbot.excommonmodule.model.Menu

/**
 * 주문하기
 * */
interface OrderInterface{
    fun goOrder()
}