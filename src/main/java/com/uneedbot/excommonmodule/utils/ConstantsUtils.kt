package com.uneedbot.excommonmodule.utils


/**
 * Created by kimjinmo on 2016. 11. 10..
 * 앱에서 사용하는 모든 상수를 관리하는 부분
 * FILENAME이 파일단위
 * KEY는 항목단위
 *
 */

object ConstantsUtils {

    ///로그인 관련
    val PREF_FILENAME_LOGIN = "pref_filename_login"
    val PREF_FILENAME_GAME_WHOLE = "pref_filename_game_whole"   //모든 게임 목록
    val PREF_FILENAME_GAME_ALL = "pref_filename_game_all"   //해당 매장에서만 사용, 모두의 게임
    val PREF_FILENAME_GAME_VERSUS = "pref_filename_game_versus"//해당 매장에서만 사용. 1:1
    val PREF_FILENAME_GAME_TABLE = "pref_filename_game_table"//해당 매장에서만 사용. 테이블내 게임

    /// id, pw, tableNo
    val PREF_KEY_LOGIN_ID = "pref_key_login_id"
    val PREF_KEY_LOGIN_PW = "pref_key_login_pw"
    val PREF_KEY_LOGIN_TABLENO = "pref_key_login_talbe_no"

    //accessToken, storeBranchUid, posIp
    val PREF_KEY_LOGIN_ACCESSTOKEN = "pref_key_login_accesstoken"
    val PREF_KEY_LOGIN_STOREBRANCHUID = "pref_key_login_storebranchuid"
    val PREF_KEY_LOGIN_POSIP = "pref_key_login_posIp"


    val PREF_FILENAME_TUTORIAL = "pref_filename_tutorial"
    val PREF_FILENAME_TABLESETTING = "pref_filename_tablesetting"
    val PREF_KEY_TABLESETTING_BUTTON_BLUE = "pref_key_tablesetting_button_blue"
    val PREF_KEY_TABLESETTING_BUTTON_LEFT = "pref_key_tablesetting_button_left"
    val PREF_KEY_TABLESETTING_BUTTON_USE = "pref_key_tablesetting_button_use"
    val PREF_KEY_TUTORIAL = "pref_key_tutorial"


    val COMPATIBILITYRESULT = arrayOf("남 몰래 잘 되길 바란다", "진실한 사랑입니다", "기다리고 기다린다", "서로 사랑하는 사이", "혼자하는 사랑", "죽어서도 너만 바라봐", "마음과는 정반대로 행동한다", "좋은 사이", "사귀게 될 사이", "사랑을 위해 사는 사이", "둘이서 영원히", "좋아해", "둘이서 좋아하는 사이", "서로 기다리고 기다리는 사이", "고백을 서로 미루는 사이", "조건이 없는 사랑", "여자분이 아깝네요", "천생연분이에요", "좋은 사랑 하세요", "끝까지 영원히", "예쁜 사랑 이어가세요", "서로 좋은 인연이에요", "첫사랑인가봐요", "헌신적인 사랑이에요", "연인까지 Go Go", "서로 좋은 연인인게 티가 나네요", "가슴에 남는 사랑 이어가세요", "Forever", "서로가 서로에게 한번씩", "좋아만 하지 말고 한 발 더 나아가 보세요", "기다림은 No No!", "용기있는 자가 사랑을 이룬답니다", "조건 없는 사랑만큼 아름다운게 없죠")

}