package com.uneedbot.excommonmodule.utils

object QueueUtils {


    private fun baseUrl(posIp: String): String {
        return "http://$posIp:9626"
    }



    public fun getPosUrl(posIp: String): String {
        return this.baseUrl(posIp)
    }


    /**
     * 주문 큐 상태 체크
     */
    fun checkQueueState(posIp: String): String {
        return baseUrl(posIp) + "/health"
    }

    /**
     * 주문
     */
    fun order(posIp: String): String {
        return baseUrl(posIp) + "/api/remote/order"
    }

    /**
     * 테이블 인원 셋팅
     */
    fun setTableMember(posIp: String, tableNo: String): String {
        return baseUrl(posIp) + "/data/v2/tables/" + tableNo
    }

    /**
     * 테이블 셋팅 요청
     */
    fun request(posIp: String): String {
        return baseUrl(posIp) + "/data/notification"
    }

    /**
     * fcm 토큰 등록
     */
    fun registrationToken(posIp: String, tableNo: String): String {
        return baseUrl(posIp) + "/data/tables/" + tableNo + "/registrationToken"
    }

    /**
     * 테이블 정보 가져오기
     */
    fun getTableInfo(posIp: String, tableNo: String): String {
        return baseUrl(posIp) + "/data/v2/tables/" + tableNo
    }

    /**
     * 매장내 모든 테이블 정보 가져오기
     */
    fun getAllMapInfo(posIp: String): String {
        return baseUrl(posIp) + "/data/v2/tables"
    }


    /**
     * 주방용 메뉴 주문 목록 가져오기
     */
    fun getKitchenOrderList(posIp: String): String {
        return baseUrl(posIp) + "/data/kitchen/orders"
    }

}
